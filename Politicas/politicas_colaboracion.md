# Políticas para la Colaboración en Canaima GNU/Linux #

# Colabora con Canaima #

Canaima GNU/Linux es un proyecto de software libre en el cual todos y todas están invitados a colaborar de forma activa según sus conocimientos y disposición. 

# Colaboración técnica #

# 1.- Reporta fallos #

Prueba la distribución y reporta los fallos encontrados, esto ayudará a los desarrolladores a mejorar el sistema operativo antes de su próxima actualización.

# 2.- Detecta y resuelve las fallas #

Si conoces la solución a alguna falla presente en la distribución Canaima GNU/Linux puedes colaborar solucionando dicha falla (dentro de tu conocimiento técnico), también puedes reportarlo al equipo desarrollador para su solución e inclusión en la próxima actualización del sistema.

# 3.- Ayuda a crear #

Elabora tematizaciones de entornos, fondos de pantalla e iconos para la distribución Canaima GNU/Linux. Para la colaboración de imágenes relacionadas al sistema, es necesario que el arte tenga alguna licencia libre (ejemplo CC - BY – SA).

# ¿Cómo colaborar si no soy técnico? #

# 1.- Sigue nuestras redes sociales #

Una forma de apoyar al proyecto Canaima GNU/Linux es siguiendo sus redes sociales y difundiendo a través de tus redes, blogs y canales de información con tus amigos y seguidores sobre los beneficios del uso de las tecnologías libres, ayudando así al crecimiento de nuestra comunidad de usuarios para seguir compartiendo conocimiento. 

- Twitter: @CanaimaGNULinux
- Mastodon: @canaimagnulinux 
- Instagram: @canaima_gnu_linux
- Telegram: t.me/CanaimaGNULinux (Canal comunitario) / t.me/ColaboracionTecnicaCanaimaVe (Canal de colaboración técnica)

# 2.- Sugiere mejoras #

Prueba la distribución y comenta a los desarrolladores tus sugerencias para mejorar en cada versión de Canaima GNU/Linux.

# 3.- Documenta # 

Si eres un investigador o tienes conocimientos en el campo, puedes realizar tu aportes de documentación histórica, técnica o recetas para la resolución de algún incidente mediante un documento paso a paso. 

# 4.- Edita, actualiza y traduce los artículos de la Wikipedia # 

Ayúdanos a mantener actualizados los artículos (inglés y español) de la distribución Canaima GNU/Linux. Si hablas algún idioma distinto a los mencionados, puedes colaborar traduciendo los artículos de la Wikipedia para llegar a mas usuarios, de esa forma Canaima GNU/Linux podrá ser conocido en nuevas fronteras.

Puedes ver los actuales artículos de Canaima en: 

https://es.wikipedia.org/wiki/Canaima_(distribucion_Linux) 
https://en.wikipedia.org/wiki/Canaima_(operating_system) 

# 5.- Diseña # 

¿Eres diseñador (a) gráfico o entusiasta del diseño? Entonces puedes aportar imágenes para afiches, infografías, entre otros. 

# 6.- Únete a nuestra comunidad # 

¡Trabajemos en equipo! 

La comunidad de Software Libre es un lugar para compartir conocimiento con usuarios, desarrolladores, activistas de la cultura, el conocimiento y las tecnologías libres. Únete y colabora ayudando a los nuevos usuarios a integrarse, ayudando a establecer una comunidad mas fuerte y activa.

# 7.- Respeta las diferencias #

En Canaima GNU/Linux estamos comprometidos con la diversidad, por eso pedimos respeto a todas las personas independientemente de su raza, creencias religiosas, identidad de género, cultura, posturas políticas, sexualidad y nacionalidad.

El respeto a las opiniones de todos ayuda a mantener un sano ambiente en la comunidad motivando e incentivando el trabajo en equipo. ¡Todos somos Canaima! 