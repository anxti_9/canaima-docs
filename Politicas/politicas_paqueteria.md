# Políticas para la inclusión de paquetería en la Distribución Nacional Canaima GNU/Linux #

# Características de los paquetes que se postulen para ser incluidos en la distribución Canaima GNU/Linux o en el Repositorio de la misma # 

- Deben tener todas las dependencias especificadas.
- El código debe estar documentado. (Documentación para usuarios finales y documentación del código).
- El paquete debe estar licenciado GPL.
- No debe ser excesivamente pesado, peso máximo 200Mb (sugerido).
- Para paquetes de tematización, estos deben funcionar en los entornos de escritorios Gnome y Xfce.
- El dueño del paquete debe mantenerlo por un periodo mínimo de dos años.
- Los paquetes deben ser multiarquitectura (amd64 y i386).
- El formato de los paquetes debe ser .deb.

NOTA: Todos los paquetes que sean postulados deben ser enviados con la documentación incluida, sus scripts de pre y post instalación, sus archivos de configuración inicial y el código fuente con todo lo necesario si está destinado a ser compilado. 

# Proceso de selección #

    Los paquetes enviados serán evaluados por el equipo técnico del Proyecto Canaima GNU/Linux para su aprobación. 


NOTA: La postulación de un paquete no asegura su inclusión en alguna de las versiones del sistema operativo Canaima GNU/Linux.

